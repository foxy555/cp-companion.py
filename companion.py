#!/bin/env python3
import argparse
import concurrent.futures
import configparser
import json
import signal
import subprocess
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from pathlib import Path

import psutil
from appdirs import AppDirs


class Color:
    reset = '\033[0m'
    bold = '\033[01m'
    disable = '\033[02m'
    underline = '\033[04m'
    reverse = '\033[07m'
    strikethrough = '\033[09m'
    invisible = '\033[08m'
    black = '\033[30m'
    red = '\033[31m'
    green = '\033[32m'


def download_data():
    data = None

    class ServerHandler(BaseHTTPRequestHandler):
        def do_POST(self):
            nonlocal data
            data = json.load(self.rfile)

    server = HTTPServer(("localhost", 1327), ServerHandler)
    server.handle_request()
    return data


def load_config():
    directory = AppDirs("companion.py", "Waaangyi")
    # load compile flags from config file
    config_path = Path(directory.user_config_dir)

    if not Path.exists(config_path):
        Path.mkdir(config_path, parents=True)
    config_path = config_path / "config.ini"

    config = configparser.ConfigParser()
    config.read(config_path)
    if not (config.has_section("global") and config.has_section("debug") and config.has_section("production")):
        config["global"] = {"header_dir": input("Input absolute directory for header bits/stdc++.h: "),
                            "editor_cmd": "vi"}
        config["debug"] = {"output_dir": "/tmp",
                           "flags": "-g",
                           "header_comp_dir": "/tmp/companion/debug"}
        config["production"] = {"output_dir": "/tmp",
                                "flags": "-g",
                                "header_comp_dir": "/tmp/companion/production"}
        with config_path.open("w") as configfile:
            config.write(configfile)

    return config


def load_problem(p):
    path = Path(p)
    json_path = path.parent / (str(path.stem) + "-unit-test.json")

    if not json_path.exists():
        print("Listening on port 1327 for test data")
        data = download_data()
        with json_path.open("w") as f:
            json.dump(data, f, indent=4)

    with json_path.open("r") as f:
        data = json.load(f)
        return data


def check_headers():
    config = load_config()
    prod_header_path = Path(config["production"]["header_comp_dir"]) / "bits" / "stdc++.h.gch"
    debug_header_path = Path(config["debug"]["header_comp_dir"]) / "bits" / "stdc++.h.gch"
    if not prod_header_path.exists() or not debug_header_path.exists():
        return False
    return True


def compile_headers():
    config = load_config()

    prod_header_src = Path(config["global"]["header_dir"])
    debug_header_src = Path(config["global"]["header_dir"])

    prod_header_dest = Path(config["production"]["header_comp_dir"]) / "bits"
    debug_header_dest = Path(config["debug"]["header_comp_dir"]) / "bits"
    prod_header_dest.mkdir(parents=True)
    debug_header_dest.mkdir(parents=True)
    prod_header_dest /= "stdc++.h.gch"
    debug_header_dest /= "stdc++.h.gch"

    debug_flags = config["debug"]["flags"].split(",")
    prod_flags = config["production"]["flags"].split(",")
    debug_flags.extend(("-o", str(debug_header_dest), str(debug_header_src)))
    prod_flags.extend(("-o", str(prod_header_dest), str(prod_header_src)))

    print("Compiling headers with debug flags")
    p = subprocess.run(["g++"] + [i.strip() for i in debug_flags])
    if p.returncode != 0:
        print("Header failed to compile with debug flags")
        return False
    print("Compiling headers with production flags")
    p = subprocess.run(["g++"] + [i.strip() for i in prod_flags])
    if p.returncode != 0:
        print("Header failed to compile with production flags")
        return False
    return True


def compile_code(path):
    config = load_config()
    flags = config["debug"]["flags"]
    output_dir = Path(config["debug"]["output_dir"]) / path.stem
    flag_args = flags.split(",")
    # If you need to use more than 256 MB of ram, you are screwed anyway, and should fix your program
    flag_args.extend(("-o", str(output_dir), "-I", config["debug"]["header_comp_dir"], str(path),
                      "-Wl,-z,stack-size=268435456"))
    p = subprocess.run(["g++"] + [i.strip() for i in flag_args])
    if p.returncode != 0:
        print("Compilation failed with code {}, execution halted".format(p.returncode))
        return False, None
    return True, output_dir


def all_tests(p_unit_test, p_binary):
    data = load_problem(p_unit_test)

    time_limit = data["timeLimit"] / 1000
    test_counter = 1
    for test in data["tests"]:
        print("Running on test {}".format(test_counter), end="")
        result = run_test(test["input"], time_limit, p_binary)
        return_code = result["return_code"]
        output = result["output"]
        time_elapsed = result["time"]
        memory = result["memory"]
        if memory >= data["memoryLimit"]:
            print(Color.red, "\tMLE", Color.reset, end="")
        if return_code == 0:
            if not compare_output(output.strip(), test["output"].strip()):
                print(Color.red, "\tWA", Color.reset)
                output_trimmed = trim_output(output.strip())
                answer_trimmed = trim_output(test["output"].strip())

                if len(output_trimmed) == len(answer_trimmed):
                    for o, a in zip(output_trimmed, answer_trimmed):
                        if o != a:
                            print(Color.red, o, Color.reset)
                        else:
                            print(Color.green, o, Color.reset)
                else:
                    print(output.strip())
            else:
                print(Color.green, "\tAC", Color.reset)
        elif return_code == -1:
            print(Color.red, "\tTLE", Color.reset)
            print(output.strip())
        else:
            print(Color.red, "\tRTE: {} ({})".format(signal.strsignal(return_code), return_code), Color.reset)
            print(output.strip())
        print("{0:0.0f} ms     {1:0.3f} MB".format(time_elapsed, memory))
        test_counter += 1


def trim_output(output):
    ret = []
    for line in output.splitlines():
        ret.append(line.strip())
    return ret


def compare_output(output1, output2):
    output1_trimmed = trim_output(output1)
    output2_trimmed = trim_output(output2)
    return output1_trimmed == output2_trimmed


def get_max_memory(pid, time_limit):
    start_time = time.time()
    max_memory = 0
    try:
        max_memory = max(psutil.Process(pid).memory_info().rss, max_memory)
    finally:
        return max_memory / 1024 ** 2


def run_test(test_case, time_limit, binary_path, custom=False):
    # config = load_config()

    start_time = time.time()
    if not custom:
        p = subprocess.Popen([binary_path], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    else:
        p = subprocess.Popen([binary_path], stdin=subprocess.PIPE)

    executor = concurrent.futures.ThreadPoolExecutor()
    memory_task = executor.submit(get_max_memory, p.pid, time_limit)

    output = ""
    return_code = 0
    try:
        stdout, stderr = p.communicate(input=test_case.encode(), timeout=time_limit)
        output = stdout.decode()
        return_code = abs(p.returncode)
    except subprocess.TimeoutExpired as e:
        return_code = -1
        p.kill()
        output = e.stdout.decode()
    finally:
        time_elapsed = time.time() - start_time
        return {"return_code": return_code, "time": time_elapsed * 1000, "output": output,
                "memory": memory_task.result()}


def new_custom_test(p_test, p_binary, temp=False):
    config = load_config()
    path = Path(p_test)
    test_path = path.parent / (str(path.stem) + "-custom-test.txt")
    if temp:
        test_path = Path("/tmp/temp-custom-test.txt")
    try:
        subprocess.run(config["global"]["editor_cmd"] + " " + str(test_path), shell=True)
    except Exception as e:
        print(e)
    custom_test(p_test, p_binary, temp)


def custom_test(p_test, p_binary, temp=False):
    problem_data = load_problem(p_test)
    path = Path(p_test)
    test_path = path.parent / (str(path.stem) + "-custom-test.txt")
    if temp:
        test_path = Path("/tmp/temp-custom-test.txt")
    with test_path.open("r") as f:
        test_data = "".join(f.readlines())
    print(test_data)
    time_limit = problem_data["timeLimit"] / 1000
    print("Running on custom test", end="")
    result = run_test(test_data, time_limit, p_binary, custom=True)
    return_code = result["return_code"]
    time_elapsed = result["time"]
    memory = result["memory"]
    output = result["output"]
    if return_code == 0:
        print(Color.green, "\tOK", Color.reset)
        print(output.strip())
    elif return_code == -1:
        print(Color.red, "\tTLE", Color.reset)
        print(output.strip())
    else:
        print(Color.red, "\tRTE: {} ({})".format(signal.strsignal(return_code), return_code), Color.reset)
        print(output.strip())
    print("{0:0.0f} ms     {1:0.3f} MB".format(time_elapsed, memory))


def vanilla(output_dir):
    subprocess.run([output_dir])


if __name__ == "__main__":
    # -a all tests
    # -l last custom test
    # -c new custom test
    # -t temporary custom test
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", action="store_true", dest="all_tests")
    parser.add_argument("-l", action="store_true", dest="last_custom")
    parser.add_argument("-c", action="store_true", dest="new_custom")
    parser.add_argument("-t", action="store_true", dest="temp_custom")
    parser.add_argument("-v", action="store_true", dest="vanilla")
    parser.add_argument("path")

    args = parser.parse_args()

    if not check_headers():
        print("Precompiled header not found, attempting to compile")
        if not compile_headers():
            exit(1)

    compile_result = compile_code(Path(args.path))
    if not compile_result[0]:
        exit(1)
    if args.all_tests:
        all_tests(args.path, compile_result[1])
        exit(0)
    if args.last_custom:
        custom_test(args.path, compile_result[1])
        exit(0)
    if args.new_custom:
        new_custom_test(args.path, compile_result[1])
        exit(0)
    if args.temp_custom:
        new_custom_test(args.path, compile_result[1], temp=True)
        exit(0)
    if args.vanilla:
        vanilla(compile_result[1])
        exit(0)
